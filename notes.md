# Simon's Notes

## Setting up the React+Redux+Typescript Features

Install by typing in the following commands at the root of your project:

```shell
npm i -S react-redux
./nod_modules/.bin/typings install -S react-redux
```

Go to **system.js**

```tsx

System.config({
    ...
    map: {
        'app':  'js',
        'redux': 'npm/redux/dist/redux.min.js',
        'react': 'npm/react-dom/dist/react-dom.min.js',
        'react-redux': 'npm/react-redux/dist/react-redux.min.js'    // add this line, check actual path
    },
    ...
})
```
## To Do

1. Custom Theme
1. Create Tabs Component
1. Redux
1. Custom Components
    - Button Bar
    - Tabs


## References

1. https://blog.risingstack.com/react-js-best-practices-for-2016/
1. https://code.visualstudio.com/Docs/languages/typescript
1. https://reactjs.org/
1. https://reactjs.org/tutorial/tutorial.html
   - https://developer.mozilla.org/en-US/docs/Web/JavaScript/A_re-introduction_to_JavaScript
   - https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects

1. http://jamesknelson.com/learn-raw-react-ridiculously-simple-forms/
1. http://redux.js.org/
   - https://www.toptal.com/react/react-redux-and-immutablejs
1. https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md
1. https://mva.microsoft.com/en-us/training-courses/using-react-with-redux-17730?l=
1. https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import
1. http://2ality.com/2014/09/es6-modules-final.html
1. https://memorynotfound.com/spring-boot-spring-data-jpa-hibernate-h2-web-console/

## Redux
1. https://github.com/reactjs/redux
1. https://egghead.io/series/getting-started-with-redux
1. https://egghead.io/lessons/react-redux-avoiding-object-mutations-with-object-assign-and-spread
    a. Object.assign( {}, original, {completed: !todo.completed} );
    a. { ...original, completed: !todo.completed }  - Babel stage 2 preset
1. https://redux-form.com/7.1.2/examples/material-ui/
```shell
npm install --save redux react-redux redux-form
npm install --save-dev redux-devtools deep-freeze
```

## Material Design Lite Information
1. https://stackoverflow.com/questions/31709013/material-design-lite-react-issues-with-tooltips

1. https://material-ui-next.com/
1. https://material-ui-next.com/getting-started/installation/

1. https://codesandbox.io/s/4j7m47vlm4?from-embed

## Router Information
1. https://reacttraining.com/react-router/web/guides/philosophy
1. https://medium.com/@pshrmn/a-simple-react-router-v4-tutorial-7f23ff27adf

## Test Resource
1. https://babeljs.io/repl/


## Videos on forms
1. https://www.youtube.com/watch?v=qH4pJISKeoI
