import React from 'react';              // eslint-disable-line no-unused-vars
import {
    BrowserRouter as Router,
    Switch,
    Route
} from 'react-router-dom';

import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';
import indigo from 'material-ui/colors/indigo';
import red from 'material-ui/colors/red';
import blueGrey from 'material-ui/colors/blueGrey';

import Header from './tags/Header';
import UserList from './components/UserList';
import UserEntry from './components/UserEntry';
import PageNotFound from './components/PageNotFound';

import { Provider } from 'react-redux';
import store from './model/store';

import ElementParser from './util/ElementParser';

import './App.css';

const theme = createMuiTheme({
    palette: {
        primary: indigo,
        secondary: blueGrey,
        error: red
    }
});

class App extends React.Component {

    element = null;

    handleClick = ( e ) => {
        let parser = new ElementParser();
        console.log( parser.dehydrate( this.element ) );
    }


    render= () => {
        this.element =  (
            <Provider store={store}>
                <MuiThemeProvider theme={theme}>
                    <div>
                        <Header onClick={this.handleClick}></Header>
                        <Router>
                            <Switch>
                                <Route exact path='/' component={UserList} />
                                <Route path='/user-entry/:userId?' component={UserEntry} />
                                <Route path='/userlist' component={UserList} />
                                <Route component={PageNotFound} />
                            </Switch>
                        </Router>
                    </div>
                </MuiThemeProvider>
            </Provider>
        );



        return this.element;
    }
}

export default App;
