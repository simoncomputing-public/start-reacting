import React from 'react';
import sadFace from './sad.jpg';

import { withStyles } from 'material-ui/styles';

const styles = {
    message: {
        padding: '10px',
    },
};

class PageNotFound extends React.Component {

    render() {
        return (
            <div className={this.props.classes.message}>
                <img alt='sad face' src={sadFace}/>
                <h4>Aww Snap, We can't find your page.</h4>
            </div>
        );
    }
}

export default withStyles(styles)(PageNotFound);