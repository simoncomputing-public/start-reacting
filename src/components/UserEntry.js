import React, { Component } from 'react';

import {  reduxForm } from 'redux-form';

import { withStyles } from 'material-ui/styles';
import Paper from 'material-ui/Paper';
import Button from 'material-ui/Button';

import MdTextField from '../tags/MdTextField';
import MdSelect from '../tags/MdSelect';

import Slide from 'material-ui/transitions/Slide';

import userService from '../services/UserService';
import { roles } from '../model/User';


const styles = theme => ({
    textField: {
        marginTop: theme.spacing.unit * 2,
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 300,
    },
    formMargins: {
        marginLeft: 'auto',
        marginRight: 'auto',
        width: 300 + theme.spacing.unit * 2
    },
    ulHorizontal: {
        padding: '10px',
        overflow: 'hidden',
        float: 'right'
    },
    liRight: {
        paddingLeft: '10px',
        display: 'inline',
    },
    errorText: {
        color: 'red'
    }
});

class UserEntry extends Component {

    state = {
        open: false,
        transition: null,
        errorMsg: '',
        userId: undefined
    };

    componentDidMount = () => {
        this.handleInitialize();
    }

    handleInitialize = () => {
        const { match, initialize } = this.props;
        const { params } = match;

        if ( params.userId ) {
            const values = userService.getUser( params.userId );
            if ( values ) {
                this.setState( {userId: params.userId });
                initialize( values );
            } else {
                this.setState( { errorMsg: 'Invalid User ID: ' + params.userId } );
            }
        }
    }

    handleSubmit = (values) => {
        let error = this.state.userId ? userService.updateUser( values ) : userService.addUser(values);
        if ( !error )
            this.props.history.push('/');

        this.setState( { errorMsg: error } );
        this.TransitionUp();
    }

    handleCancel = () => {
        const { history, reset } = this.props;
        reset();
        history.push('/');
    }

    TransitionUp = () => (<Slide direction="up"/>);

    render() {
        const { classes, handleSubmit, pristine, submitting } = this.props;
        const { errorMsg } = this.state;
        const { userId } = this.state;
        return (
            <Paper>
                <div>
                    <h4>
                        { userId ? (<span>Updating User Entry: {userId}</span>) : (<span>New User Entry</span>) }
                    </h4>
                    <form onSubmit={handleSubmit(this.handleSubmit)}>
                        <div className={classes.formMargins}>
                            <MdTextField name="userId" label="User ID" className={classes.textField} disabled={userId}/><br />
                            <MdTextField name="first" label="First Name" className={classes.textField} /><br />
                            <MdTextField name="last" label="Last Name" className={classes.textField} />
                            <MdSelect name="role" label="Role" className={classes.textField}>
                                <option value="" />
                                {roles.map((item, index) => (<option key={index} value={item}>{item}</option>))}
                            </MdSelect>

                            <br />
                            <br />
                            <br />
                            <div className={classes.errorText}>
                                {errorMsg}
                            </div>

                        </div>

                        <ul className={classes.ulHorizontal}>
                            <li className={classes.liRight}>
                                <Button raised color="primary" type="submit" disabled={pristine || submitting}>Save</Button>
                            </li>
                            <li className={classes.liRight}>
                                <Button raised onClick={event => this.handleCancel()}>Cancel</Button>
                            </li>
                        </ul>

                    </form>
                </div>
            </Paper >

        );
    }
}



export default reduxForm({ form: 'userEntry' })(
    withStyles(styles)(
        UserEntry
    )
);