import React from 'react';

import Table, {
    TableBody,
    TableCell,
    TableHead,
    TableRow,
} from 'material-ui/Table';

import { withStyles } from 'material-ui/styles';

import Paper from 'material-ui/Paper';
import IconButton from 'material-ui/IconButton';
import EditIcon from 'material-ui-icons/Edit';
import AddIcon from 'material-ui-icons/Add';
import BottomNavigation, { BottomNavigationButton } from 'material-ui/BottomNavigation';

import userService from '../services/UserService';


const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {
        minWidth: 150,
    },
    button: {
        margin: theme.spacing.unit,
        padding: '10px'
    },
});

class UserList extends React.Component {
    data = [];

    handleAdd = (event) => {
        this.props.history.push( '/user-entry' );
    }

    handleClick = (event, id) => {
        this.props.history.push( `/user-entry/${id}`);
    }

    render() {
        const { classes } = this.props;
        const data = userService.getUserList();
        return (
            <div>
                <Paper className={classes.root}>
                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow>
                                <TableCell>User Name</TableCell>
                                <TableCell>First Name</TableCell>
                                <TableCell>Last Name</TableCell>
                                <TableCell>Edit</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {data.map(n => {
                                return (
                                    <TableRow key={n.userId}>
                                        <TableCell>{n.userId}</TableCell>
                                        <TableCell>{n.first}</TableCell>
                                        <TableCell>{n.last}</TableCell>
                                        <TableCell>
                                            <IconButton className={classes.button} aria-label="Edit">
                                                <EditIcon onClick={event => this.handleClick(event, n.userId)} />
                                            </IconButton>
                                        </TableCell>
                                    </TableRow>
                                );
                            })}
                        </TableBody>
                    </Table>
                </Paper>

                <BottomNavigation className={classes.root} showLabels value="Add">
                    <BottomNavigationButton label="Add User" onClick={event =>this.handleAdd()} icon={<AddIcon />} />
                </BottomNavigation>
            </div>
        );
    }
}

export default withStyles(styles)(UserList);