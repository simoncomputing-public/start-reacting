import React from 'react';
import ReactDOM from 'react-dom';
import UserList from './UserList';
import { User } from '../model/User';
import userService from '../services/UserService';


it('renders without crashing', () => {
    let user = new User();
    user.userId = 'superman';
    user.first = 'Clark';
    user.last = 'Kent';
    user.role = 'SuperUser';

    userService.addUser(user);

    user = new User();
    user.userId = 'batman';
    user.first = 'Bruce';
    user.last = 'Wayne';
    user.role = 'PowerUser';

    userService.addUser(user);

    const div = document.createElement('div');
    ReactDOM.render(<UserList />, div);
});
