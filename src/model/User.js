
export class User {
    userId = '';
    last = '';
    first = '';
    active = true;
    role = '';
    email = '';
    startDate = '';

    // copy constructor
    constructor( that ) {
        if ( !that ) return;

        this.userId = that.userId;
        this.last  = that.last;
        this.first = that.first;
        this.active = that.active;
        this.role = that.role;
        this.email = that.email;
        this.startDate = that.startDate;
    }

    toEqual = ( that ) => {
        if ( this.userId !== that.userId ) return false;
        if ( this.last !== that.last ) return false;
        if ( this.first !== that.first ) return false;
        if ( this.active !== that.active ) return false;
        if ( this.role !== that.role ) return false;
        if ( this.email !== that.email ) return false;
        if ( this.startDate !== that.startDate ) return false;
        return true;
    }

}

export default User;
export const roles  = [ 'SuperUser', 'PowerUser', 'User', 'Guest' ];
