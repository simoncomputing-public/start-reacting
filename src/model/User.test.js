import { User } from './User';

describe('User Model', function () {
    it('creates my user', function () {
        let user = new User();

        user.last = "Kent";
        user.first = "Clark";

        expect(user.first).toEqual('Clark');
        expect(user.last).toEqual('Kent');
        expect(user.active).toBeTruthy();
    });

    it('creates with default values', function () {
        let user = new User();

        expect(user.last).toEqual('');
        expect(user.active).toBeTruthy();
    });

    it('copy constructor creates a duplicate user', function () {
        let user = new User();

        user.last = "Kent";
        user.first = "Clark";

        expect(user.first).toEqual('Clark');
        expect(user.last).toEqual('Kent');
        expect(user.active).toBeTruthy();

        let copy = new User(user);

        expect(copy.first).toEqual('Clark');
        expect(copy.last).toEqual('Kent');
        expect(copy.active).toBeTruthy();

    });

    it( 'should compare equal items' ), function() {
        let batman = new User();
        batman.userId = 'batman';
        batman.first = 'Bruce';
        batman.last = 'Wayne';
        batman.role = 'PowerUser';

        let user = new User( batman );
        
        expect( batman.toEqual( user )).toBeTruthy();
    }

    it( 'toEqual() should reject unequal items' ), function() {
        let batman = new User();
        batman.userId = 'batman';
        batman.first = 'Bruce';
        batman.last = 'Wayne';
        batman.role = 'PowerUser';

        let user = new User( batman );

        user.userId = 'bogus';       
        expect( batman.toEqual( user )).toBeFalsy();
        user.userId = batman.userId;

        user.first = 'bogus';       
        expect( batman.toEqual( user )).toBeFalsy();
        user.first = batman.first;

        user.last = 'bogus';       
        expect( batman.toEqual( user )).toBeFalsy();
        user.last = batman.last;

        user.role = 'bogus';       
        expect( batman.toEqual( user )).toBeFalsy();
        user.role = batman.role;

        user.active = 'bogus';       
        expect( batman.toEqual( user )).toBeFalsy();
        user.active = batman.active;

        user.email = 'bogus';       
        expect( batman.toEqual( user )).toBeFalsy();
        user.email = batman.email;

        user.startDate = 'bogus';       
        expect( batman.toEqual( user )).toBeFalsy();
        user.startDate = batman.startDate;

    }
    

});