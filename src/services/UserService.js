import { User } from '../model/User';

class UserService {
    instanceId = new Date().getTime();

    data = [];

    addUser( user ){
        if ( !user ) return 'User must not be null on userService.addUser()';
        if ( !user.userId ) return 'userId must not be null on userService.addUser()';

        if ( this.getUser( user.userId )) {
            return 'User already exists: ' + user.userId;
        }

        this.data.push( user );
        return null;
    }

    deleteUser( userId ) {
        if ( !userId ) return 'userId must not be null when calling userService.delete()';

        const index = this.data.findIndex( u => u.userId === userId );
        if ( index < 0 ) return 'User must exist to delete it.  userId=' + userId;

        this.data.splice( index, 1 );
        return null;
    }

    updateUser( user ) { 
        if ( !user ) return 'User must not be null on userService.updateUser()';
        if ( !user.userId ) return 'userId must not be null on userService.updateUser()';

        const index = this.data.findIndex( u => u.userId === user.userId );
        if ( index < 0 ) return 'User must exist to update it.  userId=' + user.userId;

        this.data[index] = user;
        return null;
    }

    // returns a copy, preventing direct modification of the data.
    getUser( userId ) {
        const user = this.data.find( currentUser => currentUser.userId === userId );
        if ( !user ) return null;

        return new User( user );
    }

    getUserList() { 
        return this.data.map( u => new User( u ) );
    }

    // For testing purposes
    resetList() {
        this.data = [];
    }

}

let userService = new UserService();
export default userService;