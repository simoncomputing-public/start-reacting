import userService from './UserService';
import { User } from '../model/User';

class Context {
    superman = '';
    batman = '';
}
describe( 'UserService', function() { 
    let context = {};

    beforeEach( function() {
        context = new Context();

        let user = new User();
        user.userId = 'superman';
        user.first = 'Clark';
        user.last = 'Kent'; 
        user.role = 'SuperUser';

        context.superman = user;

        user = new User();
        user.userId = 'batman';
        user.first = 'Bruce';
        user.last = 'Wayne';
        user.role = 'PowerUser';

        context.batman = user;
        
    });

    afterEach( function() { userService.resetList(); });

    //////////////////////////////////////////////////////////////////////////
    // userService.addUser() Tests

    it( 'should return error when attempting to add a null user', function(){
        const result = userService.addUser( null );
        expect( result ).toEqual( 'User must not be null on userService.addUser()' );        
    });

    it( 'should return an error if attempting to add user without userId', function() {
        context.superman.userId = null;
        const result = userService.addUser( context.superman );
        expect( result ).toEqual( 'userId must not be null on userService.addUser()' );
    });

    it( 'should return an error when attempting to add the same user', function(){
        let result = userService.addUser( context.superman );
        expect( result ).toBeFalsy();

        result = userService.addUser( context.superman );
        expect( result ).toContain( 'User already exists: ' );
    });

    //////////////////////////////////////////////////////////////////////////
    // userService.getUser() Tests

    it( 'should add user and get same user back', function(){
        let result = userService.addUser( context.superman );
        expect( result ).toBeFalsy();

        const user = userService.getUser( context.superman.userId );
        expect( user ).toBeDefined();

        expect( user.userId ).toEqual( context.superman.userId );
        expect( user.last ).toEqual( context.superman.last );
        expect( user.first ).toEqual( context.superman.first );
        expect( user.role ).toEqual( context.superman.role );
        expect( user.active ).toEqual( context.superman.active );
    });

    it( 'should return a null if calling getUser() with a null value', function() {
        let result = userService.getUser( null );
        expect( result ).toBeNull();

        result = userService.getUser();
        expect( result ).toBeNull();

    } );

    it ( 'should return null when calling getUser() with a userId that doesn\'t exist', function(){
        const result = userService.getUser( 'random-invalid-key' );
        expect( result ).toBeNull();
    });
    
    //////////////////////////////////////////////////////////////////////////
    // userService.deleteUser() and getUserList() Tests    
    it( 'should return error when attempting to delete null user', function(){
        const result = userService.deleteUser();
        expect( result ).toEqual( 'userId must not be null when calling userService.delete()' );
    });

    it( 'should return error when attempting to delete a non-existent user', function(){
        const result = userService.deleteUser( 'random-invalid-key' );
        expect( result ).toContain( 'User must exist to delete it.' );
    });

    it( 'should delete user', function() {
        let result = userService.addUser( context.batman );
        expect( result ).toBeNull();

        let userList = userService.getUserList();
        expect( userList.length ).toEqual( 1 );

        result = userService.addUser( context.superman );
        expect( result ).toBeNull();

        userList = userService.getUserList();
        expect( userList.length ).toEqual( 2 );
        
        result = userService.deleteUser( context.batman.userId );
        expect( result ).toBeNull();

        userList = userService.getUserList();
        expect( userList.length ).toEqual( 1 );
        
        const user = userService.getUser( context.batman.userId );
        expect( user ).toBeNull();
    });
        

    //////////////////////////////////////////////////////////////////////////
    // userService.userUpdate() tests
    it( 'should update a user entry', function(){
        let result = userService.addUser( context.batman );
        expect( result ).toBeNull();

        let userList = userService.getUserList();
        expect( userList.length ).toEqual( 1 );

        userList[0].role = 'SuperUser';
        result = userService.updateUser( userList[0] );
        expect( result ).toBeFalsy();

        let user = userService.getUser( context.batman.userId );
        expect( user.role ).toEqual( 'SuperUser' );
    });

    it( 'should not update a null user entry', function(){
        let result = userService.addUser( context.batman );
        expect( result ).toBeNull();

        let userList = userService.getUserList();
        expect( userList.length ).toEqual( 1 );

        userList[0].role = 'SuperUser';
        result = userService.updateUser();
        expect( result ).toContain( 'User must not be null on userService.updateUser()' );

        userList[0].userId = null;
        result = userService.updateUser( userList[0] );
        expect( result ).toContain( 'userId must not be null on userService.updateUser()' );

        userList[0].userId = 'bogus ID';
        result = userService.updateUser( userList[0] );
        expect( result ).toContain( 'User must exist to update it.  userId=' );        

    });

}); 