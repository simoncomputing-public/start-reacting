import React from 'react';              // eslint-disable-line no-unused-vars

import { withStyles } from 'material-ui/styles';

import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';

import IconButton from 'material-ui/IconButton';
import MenuIcon from 'material-ui-icons/Menu';


const styles = theme => ({
    root: {
        marginTop: 0,
        width: '100%',
    },
    flex: {
        flex: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },

});

class Header extends React.Component {

    render() {
        const { classes, onClick } = this.props;

        return (
            <div className={classes.root}>
                <AppBar position="static">
                    <Toolbar>
                        <IconButton className={classes.menuButton} color="contrast" aria-label="Menu">
                            <MenuIcon onClick={onClick} />
                        </IconButton>
                        <Typography type="title" color="inherit" className={classes.flex}>
                            Example
                        </Typography>

                    </Toolbar>
                </AppBar>
            </div>
        );
    }
}


export default withStyles(styles)(Header);