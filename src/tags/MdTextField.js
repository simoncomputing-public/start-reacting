import React, { Component } from 'react';
import { Field } from 'redux-form';
import TextField from 'material-ui/TextField';

const renderTextField = ({
    input,
    label,
    className,
    disabled,
    meta: { touched, error },
    ...custom
}) => (
    <TextField
        className={className}
        label={label}
        disabled={disabled}
        {...input}
        {...custom}
    />
);

class MdTextField extends Component {

    render() {
        const { name, label, className } = this.props;
        return (
            <Field name={name} label={label} className={className} component={renderTextField}/>
        );
    }

}

export default MdTextField;
