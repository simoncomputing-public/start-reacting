import React from 'react';              // eslint-disable-line no-unused-vars
import ReactDOM from 'react-dom';       // eslint-disable-line no-unused-vars

class ElementParser {

    parseChildren = ( children ) => {
        if ( !children ) return null;

        if ( typeof( children ) === 'string' )
            return ', children: "' + children + '"';

        if ( !children[Symbol.iterator] ) {

            return ', children: [ ' + this.dehydrate( children ) + ']';
        }

        let json = ', children: [ ';
        let first = true;
        for ( let child of children ) {
            if ( !first ) json += ', ';

            json += this.dehydrate( child );
            first = false;
        }
        json += ' ]';
        return json;
    }

    dehydrate = ( element ) => {
        if ( !element ) return;
        if ( typeof( element ) === 'string' ) {
            return '"' + element + '"';
        }

        if ( !element.type ) return;

        let localType = typeof( element.type ) === 'string' ? element.type : element.type.name;

        let json = '{ type: "' + localType + '"' ;
        let children = null;
        for (let key in element.props) {
            // save for processing after all properties have been handled
            if (key === 'children') {
                children = element.props[key];
                continue;
            } else if ( key === 'store' ) {
                continue;
            } else if ( key === 'component' ) {

                continue;
            } else if ( key.startsWith( 'on' )) {
                continue;
            }

            if ( key === 'Route' && element.props[key].path.contains( 'user-entry') ) {
                console.log( JSON.stringify(element.props[key] ));
            }


            json += ', ' + key + ': "' + element.props[key] + '"';

        }

        if ( children ) {
            json += this.parseChildren( children );
        }

        json += ' }';
        console.log( '########################' + json );
        return json;

    }
}

export default ElementParser;
