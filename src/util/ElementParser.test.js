import React from 'react';               // eslint-disable-line no-unused-vars
import ReactDOM from 'react-dom';        // eslint-disable-line no-unused-vars
import ElementParser from './ElementParser';

describe( 'extract element', function() {
    it( 'should extract a single div element', function() {
        let el = <div></div>;
        let parser = new ElementParser();

        const result = parser.dehydrate( el );
        expect( result ).toEqual( '{ type: "div" }' );
    });

    it( 'should extract a div with text', function() {
        let el = <div>test</div>;
        let parser = new ElementParser();

        const result = parser.dehydrate( el );
        expect( result ).toEqual( '{ type: "div", children: "test" }' );

    });

    it( 'should extract a div with properties', function() {
        let el = <div className="test"></div>;
        let parser = new ElementParser();

        const result = parser.dehydrate( el );
        expect( result ).toEqual( '{ type: "div", className: "test" }' );

    });

    it ( 'should extract multiple children in div', function() {
        let el = ( <div className="test">text before<p>stuff in paragraph</p>text after</div> );
        let parser = new ElementParser();

        const result = parser.dehydrate( el );
        expect( result ).toEqual( '{ type: "div", className: "test", ' +
          'children: [ "text before", { type: "p", children: "stuff in paragraph" }, ' +
          '"text after" ] }' );
    });

});
